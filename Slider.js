//USER SETTINGS
var countClass = "";		//Slider Contents Being Counted
var divID = "";				//ID of Slider
var buttonBID = "";			//Forward Button ID
var buttonFID = ""; 		//Back Button ID
var desktopCount = 3; 		//Groups of Items Visible At Once Minus 1 (Default: 3)
var desktopPercent = 11;	//Percent Move on Desktop (Default: 11)
var mobilePercent = 16;		//Percent Move on Mobile (Default: 16)
//SYSTEM
var deviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
var percent = 0;
var sliderDiv = document.getElementById(divID);
var count = document.getElementsByClassName(countClass).length;
var bButton = document.getElementById(buttonBID);
var fButton = document.getElementById(buttonFID);
var attempts = 0;
function setupSlider() {
	if (deviceWidth > 767) {
		bButton.addEventListener('click', function() {moveSlider("l", desktopCount, desktopPercent);}, false);
		fButton.addEventListener('click', function() {moveSlider("r", desktopCount, desktopPercent);}, false);
	}
	else {
		bButton.addEventListener('click', function() {moveSlider("l", count, mobilePercent);}, false);
		fButton.addEventListener('click', function() {moveSlider("r", count, mobilePercent);}, false);
	}
}
function moveSlider(dir, groups, percentStart) {
	if (dir == "l") {
		if (percent > 0) {
			percent -= percentStart;
		}
		else {
			percent = (groups) * percentStart;
		}
		sliderDiv.style.transform = "translate(-"+ percent +"%, 0)";
	}
	else {
		if (percent < ((groups)*percentStart)) {
			percent += percentStart;
		}
		else {
			percent = 0;
		}
		sliderDiv.style.transform = "translate(-"+ percent +"%, 0)";
	}
};
function checkSlider() {
	if (document.getElementsByClassName(countClass)[0] !== undefined) {
		count = document.getElementsByClassName(countClass).length;
		console.log("Success: Slider Div Found @" + divID);
		window.setTimeout(setupSlider, 100);
	}
	else {
		console.log("Error: Find Slider Div Missing, Trying Again");
		attempts += 1;
		if (attempts < 5) {
			window.setTimeout(checkSlider, 1000);
		}
		else {
			console.log("Fatal Error: Slider Div Not Found. Ending Attempts.")
		};
	};
};
//INIT
window.addEventListener("load", checkSlider, "false");